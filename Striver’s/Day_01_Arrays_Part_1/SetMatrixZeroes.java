/**
 * https://leetcode.com/problems/set-matrix-zeroes/description/
 * https://leetcode.com/problems/set-matrix-zeroes/submissions/926182691/
*/

class Solution {
    public void setZeroes(int[][] matrix) {
        int numRows = matrix.length;
        int numCols = matrix[0].length;

        boolean firstRowHasZero = false;
        boolean firstColHasZero = false;
        boolean topLeftValueIsZero = (matrix[0][0] == 0);

        for(int i = 0; i < numRows; i++) {
            if(matrix[i][0] == 0) {
                firstRowHasZero = true;
                break;
            }
        }

        for(int i = 0; i < numCols; i++) {
            if(matrix[0][i] == 0) {
                firstColHasZero = true;
                break;
            }
        }

        for(int i = 1; i < numRows; i++) {
            for(int j = 1; j < numCols; j++) {
                if(matrix[i][j] == 0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }

        for(int i = 1; i < numCols; i++) {
            if(matrix[0][i] == 0) {
                for(int j = 1; j < numRows; j++) {
                    matrix[j][i] = 0;
                }
            }
        }

        for(int i = 0; i < numRows; i++) {
            if(matrix[i][0] == 0) {
                for(int j = 0; j < numCols; j++) {
                    matrix[i][j] = 0;
                }
            }
        }

        if(topLeftValueIsZero || firstRowHasZero) {
            for(int i = 0; i < numRows; i++) {
                matrix[i][0] = 0;
            }
        }

        if(topLeftValueIsZero || firstColHasZero) {
            for(int i = 0; i < numCols; i++) {
                matrix[0][i] = 0;
            }
        }
    }
}
